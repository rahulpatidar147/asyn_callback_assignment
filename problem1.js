const fs = require('fs')

function pro1 (n,callBack) {

    let data = [
        { name: 'Quincy', grade: 96 },
        { name: 'Jason', grade: 84 },
        { name: 'Alexis', grade: 100 },
        { name: 'Sam', grade: 65 },
        { name: 'Katie', grade: 90 }
    ]; 
    fs.mkdir('rendomFilesDir', (err) => {
        if(err){
            callBack(err,null)
        } else {
            callBack(null,'Directory Created ')
        }
    })
    for(let index = 1; index <= n; index++ ) {

        fs.writeFile('rendomFilesDir/test'+index+'.JSON', JSON.stringify(data), (err) => { 
            if(err) {
                callBack(err,null)
            } else {
                callBack(null,'file created.')
                fs.unlink('rendomFilesDir/test'+index+'.JSON', (err) =>{
                    if(err) {
                        callBack(err,null)
                    } else {
                        callBack(null,'delete json file')
                    }
                    
                })
            }
        })
    }
} 
module.exports=pro1;
